/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leandro.kata09;


/**
 *
 * @author mirandalelo
 * @param <PriceType>
 */
public class PriceRule {
    
    private final PriceType type;
    private final String item;
    private int numberOfItems;
    private final double value;
    
    public PriceRule(String item, double value) {
        
        this.type = PriceType.UNIT;
        this.item = item;
        this.numberOfItems = 1;
        this.value = value;
        
    }
    
    public PriceRule(String item, int numberOfItems, double value) {
        
        this.type = PriceType.SPECIAL;
        this.item = item;
        this.numberOfItems = numberOfItems;
        this.value = value;
        
    }
    
    public PriceType getType() {
        return this.type;
    }
    
    public String getItem() {
        return this.item;
    }
    
    public double getValue() {
        return this.value;
    }
    
    public int getNumberOfItems() {
        return this.numberOfItems;
    }
    
}
