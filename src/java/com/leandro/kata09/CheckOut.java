/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.leandro.kata09;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author mirandalelo
 */
public class CheckOut {

	private final List<PriceRule> pricingRules;
	private final Map<String, String> cart;
	private double total;

	private CheckOut() {

		this.pricingRules = new ArrayList<PriceRule>();

		// UNIT
		this.pricingRules.add(new PriceRule("A", 50));
		this.pricingRules.add(new PriceRule("B", 30));
		this.pricingRules.add(new PriceRule("C", 20));
		this.pricingRules.add(new PriceRule("D", 15));

		// SPECIAL
		this.pricingRules.add(new PriceRule("A", 3, 130));
		this.pricingRules.add(new PriceRule("B", 2, 45));

		this.cart = new TreeMap<>();

	}

	public CheckOut(List<PriceRule> pricingRules) {

		this.pricingRules = pricingRules;
		this.cart = new TreeMap<>();

	}

	public void scan(String item) {

		if (cart.containsKey(item)) {

			cart.put(item, cart.get(item) + item);

		} else {
			cart.put(item, item);
		}

	}

	public void applyRules() {

		this.total = 0;
		List<PriceRule> itemPriceRules;

		for (Map.Entry<String, String> entry : cart.entrySet()) {

			String key = entry.getKey();
			String value = entry.getValue();

			if (value.length() == 1) {

				for (PriceRule pr : this.pricingRules) {

					if (pr.getItem().equals(key)) {
						this.total += pr.getValue();
						break;
					}

				}

			} else {

				itemPriceRules = new ArrayList<PriceRule>();
				PriceRule prUnit = null;
				PriceRule prSpecial = null;

				for (PriceRule pr : this.pricingRules) {

					if (pr.getItem().equals(key)) {
						itemPriceRules.add(pr);
					}
				}

				double totalSpecial = 0;
				double totalUnit = 0;
				int numberOfSpecialItems = 0;

				for (PriceRule spr : itemPriceRules) {

					if (spr.getType() == PriceType.SPECIAL) {

						totalSpecial = (value.length() / spr.getNumberOfItems())
								* spr.getValue();
						numberOfSpecialItems += (value.length() / spr
								.getNumberOfItems()) * spr.getNumberOfItems();

					} else {

						prUnit = spr;

					}

				}

				totalUnit = prUnit != null ? prUnit.getValue()
						* (value.length() - numberOfSpecialItems) : 0;
				this.total += totalUnit + totalSpecial;
			}
		}

	}

	public void printCart() {

		for (Map.Entry<String, String> entry : cart.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();

			System.out.println(key + " - " + value);
		}

		System.out.println("TOTAL: US$ " + this.total);

	}

	public double total() {
		return 0;
	}

	public static void main(String[] args) {

		List<PriceRule> array = new ArrayList<PriceRule>();

		// UNIT
		array.add(new PriceRule("A", 50));
		array.add(new PriceRule("B", 30));
		array.add(new PriceRule("C", 20));
		array.add(new PriceRule("D", 15));

		// SPECIAL
		array.add(new PriceRule("A", 3, 130));
		array.add(new PriceRule("B", 2, 45));

		CheckOut co = new CheckOut(array);

		co.scan("A");
		co.scan("B");

		co.applyRules();
		co.printCart();

	}

	public double getTotal() {

		return this.total;

	}

	public List<PriceRule> getPricingRules() {

		return this.pricingRules;

	}

	private static CheckOut singleton;

	public static synchronized CheckOut getInstance() {
		if (singleton == null)
			singleton = new CheckOut();
		return singleton;
	}

}
