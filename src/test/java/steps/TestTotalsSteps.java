package steps;

import com.leandro.kata09.CheckOut;
import java.util.ArrayList;
import java.util.List;

import com.leandro.kata09.PriceRule;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;

import static org.junit.Assert.*;

public class TestTotalsSteps {

    private CheckOut checkOut;

    @Given("^the list of \"(.*?)\"$")
    public void the_list_of(String arg1) throws Throwable {

        String[] items = arg1.trim().split("");
        
        List<PriceRule> array = new ArrayList<PriceRule>();

        //UNIT
        array.add(new PriceRule("A", 50));
        array.add(new PriceRule("B", 30));
        array.add(new PriceRule("C", 20));
        array.add(new PriceRule("D", 15));

        //SPECIAL
        array.add(new PriceRule("A", 3, 130));
        array.add(new PriceRule("B", 2, 45));

        checkOut = new CheckOut(array);

        for (String item : items) {
            
            checkOut.scan(item);
            
        }
    }

    @When("^we calculate the unit and special prices$")
    public void we_calculate_the_unit_and_special_prices() throws Throwable {
        
        checkOut.applyRules();
        
    }

    @Then("^it matches the \"(.*?)\"$")
    public void it_matches_the(String arg1) throws Throwable {
        
        assertEquals(arg1.trim(), String.valueOf((int)checkOut.getTotal()));
        
    }

}
