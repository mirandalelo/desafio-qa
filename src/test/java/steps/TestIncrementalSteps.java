package steps;

import static org.junit.Assert.assertEquals;

import com.leandro.kata09.CheckOut;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class TestIncrementalSteps {

	private CheckOut checkOut;

	@Given("^the itens being shopped$")
	public void the_itens_being_shopped() throws Throwable {

		checkOut = checkOut.getInstance();

	}

	@When("^continuously adding an \"(.*?)\" to the Shopping Cart$")
	public void continuously_adding_an_to_the_Shopping_Cart(String arg1)
			throws Throwable {

		checkOut.scan(arg1.trim());
		checkOut.applyRules();

	}

	@Then("^the incremented value should match the \"(.*?)\"$")
	public void the_incremented_value_should_match_the(String arg1)
			throws Throwable {

		assertEquals(arg1.trim(), String.valueOf((int) checkOut.getTotal()));

	}

}
