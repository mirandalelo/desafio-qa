Feature: Back to the Checkout 
Scenario Outline: Calcular o valor total de itens com precos unitarios e especiais 
	Given the list of "<items>" 
	When we calculate the unit and special prices 
	Then it matches the "<total_amount>" 
	
	Examples: 
	| items  | total_amount |
	|        |0             |
	|A       |50            |
	|AB      |80            |
	|CDBA    |115           |
	|AA      |100           |
	|AAA     |130           |
	|AAAA    |180           |
	|AAAAA   |230           |
	|AAAAAA  |260           |
	|AAAB    |160           |
	|AAABB   |175           |
	|AAABBD  |190           |
	|DABABA  |190           |
	