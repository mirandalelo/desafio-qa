Feature: Back to the Checkout 
Scenario Outline:
Calcular o valor total de forma incremental a cada item adicionado 
	Given the itens being shopped 
	When continuously adding an "<item>" to the Shopping Cart 
	Then the incremented value should match the "<total_amount>" 
	
	Examples: 
		| item  | total_amount |
		|A       |50           |
		|B       |80           |
		|A       |130          |
		|A       |160          |
		|B       |175          |